
import ShopItem from "../../../Index/Main/ShopItem/ShopItem";
import { useDispatch, useSelector } from "react-redux";
import Form from "../Form/Form"


import '../Styles/BasketStyles.scss'

export default function BasketPage(){

    const shopedItems = useSelector((state) => state.purchase.shopedItems);
    
    return(
        <div className="main__container">
            <h1>Your Purchases</h1>
            <div className="main__container-items">
                {shopedItems.length === 0 ? (
                    <h2>You have no boughted products yet</h2>
                ) : (
                    <div className="main__items-list">
                        {shopedItems.map((item) => (
                            <ShopItem 
                                key={item.id} 
                                id={item.id} 
                                image={item.image} 
                                name={item.name}
                                price={item.price}
                                isBasket={"true"}
                            />
                        ))}
                    </div>
                )}
            </div>
            {shopedItems.length&&
                <div className="main__checkout-container">
                <h2>Buy your purchases</h2>
                <div className="main__checkout-form">
                    <Form/>
                </div>
            </div>
            }
            
        </div>
    )
}
