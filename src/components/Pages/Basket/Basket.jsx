
import ShopItem from "../../Index/Main/ShopItem/ShopItem";
import BasketMain from "../Basket/Main/BasketMain"
import ModalImage from "../../Modal/ModalTypes/ModalImage"
import ModalCheckout from "../../Modal/ModalTypes/ModalCheckout";

import { useSelector } from "react-redux";

export default function BasketPage(){
    
    const isImageModalOpen = useSelector(state => state.modal.isImageModalOpen);
    const isCheckoutModalOpen = useSelector(state => state.modal.isCheckoutModalOpen);
    return(
        <>
            <BasketMain/>
            {isImageModalOpen&&(
                 <ModalImage/>
            )}
            {isCheckoutModalOpen&&(
                <ModalCheckout/>
            )}
        </>
        
    )
}