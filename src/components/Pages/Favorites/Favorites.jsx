
import FavoriteMain from "./Main/FavoritesMain";
import ModalText from "../../Modal/ModalTypes/ModalText";

import { useSelector } from "react-redux";

export default function FavoritePage(){
    const isModalOpen = useSelector(state => state.modal.isModalOpen);
    
    return(
        <>
        <FavoriteMain>
        </FavoriteMain>
        {isModalOpen&&(
            <ModalText/>
         )

         }   
        </>
    )
}