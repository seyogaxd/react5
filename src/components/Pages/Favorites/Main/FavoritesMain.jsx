
import ShopItem from "../../../Index/Main/ShopItem/ShopItem";

import { useDispatch, useSelector } from "react-redux";

export default function FavoriteMain(){

    const favoriteItems = useSelector((state) => state.favorite.favoriteItems);

    return(
        <div className="main__container">
            <h1>Your Favorites</h1>
            <div className="main__container-items">
                {favoriteItems.length === 0 ? (
                    <h2>You have no favorite products yet</h2>
                ) : (
                    <div className="main__items-list">
                        {favoriteItems.map((item) => (
                            <ShopItem 
                                key={item.id} 
                                id={item.id} 
                                image={item.image} 
                                name={item.name}
                                price={item.price}
                            />
                        ))}
                    </div>
                )}
            </div>
        </div>
    )
}