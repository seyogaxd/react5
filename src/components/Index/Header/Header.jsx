import LogoImage from "../../../assets/logo/Logo.png"
import ShopIcon from "../../../assets/icons/shoped-icon.svg"
import FavoriteIcon from "../../../assets/icons/favorite-icon.svg"
import { NavLink } from "react-router-dom";

import { useSelector } from "react-redux";

export default function Header()
{
    const favoriteItems = useSelector((state) => state.favorite.favoriteItems);
    const shopedItems = useSelector((state) => state.purchase.shopedItems);
    return(
        <div className="header__container">
            <div className="header__wrapper">
                <NavLink to="/">
                <div className="header__logo"><img src={LogoImage} alt="LogoImage" /></div>
                </NavLink>
                <div className="header__pages-list">
                    <ul className="header__list">
                        <li>
                            <NavLink
                            to="/"
                            className={"header__pages-list-item page-active"}
                            >
                                <span className="header__pages-text">Home</span>
                            </NavLink>
                        </li>
                        <li>
                        <NavLink
                            to="/favorites"
                            className={"header__pages-list-item page-active"}
                            >
                                <span className="header__pages-text">Favorites</span>
                            </NavLink>
                        </li>
                        <li>
                        <NavLink
                            to="/basket"
                            className={"header__pages-list-item page-active"}
                            >
                                <span className="header__pages-text">Puchases</span>
                            </NavLink>
                        </li>
                    </ul>
                </div>
                <div className="header__info">
                    <ul className="header__list">
                        <li className="header__list-item">
                            <NavLink
                            to={"/basket"}
                            className={"header__item-container"}
                            >
                                <img src={ShopIcon} alt="Shoped-icon" />
                                <span className="header__item-number">
                                    {shopedItems.length}
                                </span>
                            </NavLink>
                        </li>
                        <li className="header__list-item">
                            <NavLink
                            to={"/favorites"}
                            className={"header__item-container"}
                            >
                                <img src={FavoriteIcon} alt="Favorite-icon" />
                                <span className="header__item-number">
                                    {favoriteItems.length}
                                </span>
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
            
        </div>
    )
}