import Header from "./Header/Header"
import Main from "./Main/Main"
import './Styles/IndexStyles.scss'
import ModalText from "../Modal/ModalTypes/ModalText"
import { useSelector } from "react-redux";

export default function IndexPage(props)
{   
    const isModalOpen = useSelector(state => state.modal.isModalOpen);

    return(
        <>
        <Main/>
         {isModalOpen&&(
            <ModalText
            title={"success"}
            text={"pabeda"}
            />
         )
         }   
        </>
    )
}