import ShopItem from "../ShopItem/ShopItem";
import { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { dataFetch } from "../../../../Redux/data.slice/data.slice";

export default function ShopList(props)
{
    
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(dataFetch());
    }, [dispatch]);
    const itemsList = useSelector((state) => state.data.items);
 
    return(
        <>
        {itemsList.map((item) => (
            <ShopItem 
                key={item.id} 
                id={item.id}
                image={item.image} 
                name={item.name}
                price={item.price}
                
                />
          ))}
          </>
    )
}