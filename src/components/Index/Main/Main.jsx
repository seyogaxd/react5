import ShopList from "./ShopList/ShopList";

export default function Main(props)
{

    return(
        <div className="main__container">
            <h1>Our Products</h1>
            <div className="main__container-items">
                <div className="main__items-list">
                <ShopList/>
            </div>
            </div>
            
        </div>
    )
}