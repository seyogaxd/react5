import '../Styles/ModalStyles.scss'

export default function ModalFooter({firstText, secondaryText, firstClick, secondaryClick})
{
    return(
        <div className="main__modal-footer">
            {
                firstText && 
                <button onClick={firstClick}>{firstText}</button>
            }
            {
                secondaryText &&
                <button onClick={secondaryClick}>{secondaryText}</button>
            }
        </div>
    )
}