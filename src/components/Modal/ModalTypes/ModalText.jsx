import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody";
import ModalClose from "../ModalClose/ModalClose";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import Modal from "../Modal";
import items from "../../../../public/items.json"

import { addToShoped } from "../../../Redux/purchase.slice/purchase.slise";
import { useDispatch, useSelector } from "react-redux";
import { closeModal } from "../../../Redux/modal.slice/modal.slice";

export default function ModalText()
{
    const dispatch = useDispatch();
    const closeWindow = () =>{
        dispatch(closeModal());
    }
    
    const pressedID = useSelector(state => state.modal.modalItemId);
    const selectedItem = items.find(item => item.id === pressedID);
    return(
        <ModalWrapper>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={closeWindow}></ModalClose>
                </ModalHeader>
                <ModalBody>
                        <h2>{`Buy Product "${selectedItem.name}"`}</h2>
                        <p>{`color: ${selectedItem.color}; cost: ${selectedItem.price}; article: ${selectedItem.article}`}</p>
                </ModalBody>
                <ModalFooter
                firstText={"ADD TO BOUGHTED"}
                firstClick={() => {
                    dispatch(addToShoped(pressedID));
                    closeWindow();
                }}
                >
                </ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}