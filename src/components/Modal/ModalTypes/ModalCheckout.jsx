import ModalHeader from "../ModalHeader/ModalHeader";
import ModalBody from "../ModalBody/ModalBody";
import ModalClose from "../ModalClose/ModalClose";
import ModalFooter from "../ModalFooter/ModalFooter";
import ModalWrapper from "../ModalWrapper/ModalWrapper";
import Modal from "../Modal";

import { useDispatch, useSelector } from "react-redux";
import { closeCheckoutModal } from "../../../Redux/modal.slice/modal.slice";

export default function ModalCheckout()
{
    const dispatch = useDispatch();
    const closeWindow = () =>{
        dispatch(closeCheckoutModal());
    }
    
    return(
        <ModalWrapper>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={closeWindow()}></ModalClose>
                </ModalHeader>
                <ModalBody>
                        <h2>{`Are you sure?`}</h2>
                        {/* <p>{`color: ${selectedItem.color}; cost: ${selectedItem.price}; article: ${selectedItem.article}`}</p> */}
                </ModalBody>
                <ModalFooter
                firstText={"BUY"}
                firstClick={() => {
                    closeWindow();
                }}
                >
                </ModalFooter>
            </Modal>
        </ModalWrapper>

    )
}