import '../Styles/ModalStyles.scss'

export default function ModalHeader({ children })
{
    return(
        <div className="main__modal-header">
            {children}
        </div>
    )
}