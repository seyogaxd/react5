import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import request from '../request/request';

export const dataFetch = createAsyncThunk('data/fetch', async () => {
    const response = await request('items.json');
    return response;
});

const initialState = {
    items: [],
    status: 'idle',
    error: null,
};

const dataSlice = createSlice({
    name: 'data',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(dataFetch.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(dataFetch.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.items = action.payload;
            })
            .addCase(dataFetch.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            });
    },
});

export default dataSlice.reducer;
